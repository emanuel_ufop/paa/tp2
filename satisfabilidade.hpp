#ifndef satisfabilidade_HPP
#define satisfabilidade_HPP

using namespace std;

#include "./util/sat.hpp"

class Satisfabilidade {
    Sat *s;
public:
    Satisfabilidade(char *name);
    ~Satisfabilidade();
    void satisfabilidade();
    void info();
};

#endif